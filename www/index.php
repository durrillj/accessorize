<?PHP

// Load site configuration
require_once(realpath(dirname(__FILE__) . '/../config.php'));

// Get URL Vars
$urlVars = null;
if (isset($_GET['var'])) {
    $urlVars = explode('/', $_GET['var']);
}

// Get Page
$page = 'home';
if (!empty($urlVars[0])) {
    $page = $urlVars[0];
}

$display = new \StaticFusion\Display();
$display->output($page);
