<?PHP
namespace StaticFusion;

class Display
{
    public function output($page)
   {
        global $environment;

        // Generate Google analytics if in production environment.
        $analytics = null;
        if ($environment === 'production') {
            $analytics = Templates::genOutput('google-analytics.tpl');
        }
        $array['analytics'] = $analytics;

        switch ($page) {
            case 'home':
                $array['title'] = 'Home';
                $array['meta']  = '';
                $array['body']  = Templates::genOutput('pages/home.tpl');
                break;
            case 'about_us':
                $array['title'] = 'About Us';
                $array['meta']  = '';
                $array['body']  = Templates::genOutput('pages/about.tpl');
                break;
            case 'products':
                $array['title'] = 'Products';
                $array['meta']  = '';
                $array['body']  = Templates::genOutput('pages/products.tpl');
                break;
            case 'gallery':
                $sDir   = 'assets/img/gallery/store/';
                $pDir   = 'assets/img/gallery/products/';
                $sGroup = array();
                $pGroup = array();
                $n      = 0;
                foreach (glob($sDir.'*.jpg') as $file) {
                    $n++;
                    $string   = array('file'   => $file,
                                      'target' => 'storeImg-'.$n,
                                      'label'  => 'storeImg-'.$n.'-label'
                                );
                    $sGroup[] = Templates::genOutput('thumbnail.tpl', $string);
                }
                $n = 0;
                foreach (glob($pDir.'*.jpg') as $file) {
                    $n++;
                    $string   = array('file'   => $file,
                                      'target' => 'productImg-'.$n,
                                      'label'  => 'productImg-'.$n.'-label'
                                );
                    $pGroup[] = Templates::genOutput('thumbnail.tpl', $string);
                }
                $gal['store_thumbs']   = implode(PHP_EOL, $sGroup);
                $gal['product_thumbs'] = implode(PHP_EOL, $pGroup);
                $array['page_css']     = '<link href="/assets/css/lightbox.css" rel="stylesheet">';
                $array['title']        = 'Gallery';
                $array['meta']         = '';
                $array['body']         = Templates::genOutput('pages/gallery.tpl', $gal);
                break;
            case 'promotions':
                $array['title'] = 'Promotions';
                $array['meta']  = '';
                $array['body']  = Templates::genOutput('pages/promotions.tpl');
                break;
            case 'contact_us':
                $array['title'] = 'Contact Us';
                $array['meta']  = '';
                $map['iframe']  = Templates::genOutput('map.tpl');
                $array['body']  = Templates::genOutput('pages/contact.tpl', $map);
                break;
            case 'coupon':
                $array['page_css']  = '<link href="/assets/css/coupon.css" rel="stylesheet">';
                $array['title']     = 'Coupon';
                $array['meta']      = '';
                $array['analytics'] = '';
                $output             = Templates::genOutput('pages/coupon.tpl', $array);
                print($output);
                exit;
            default:
                $array['title'] = 'Page NOT Found';
                $array['meta']  = '';
                $array['body']  = Templates::genOutput('pages/404.tpl');
                break;
        }

        $activeNav    = array($page => 'active');
        $array['nav'] = Templates::genOutput('nav.tpl', $activeNav);
        $baseTPL      = Templates::genOutput('base.tpl', $array);
        print($baseTPL);
        exit();
    }
}
