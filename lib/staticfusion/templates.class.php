<?PHP
namespace StaticFusion;

class Templates
{
    private static function getTplFile($fileName)
    {
        $path = TPL_PATH.'/'.$fileName;
        if (!file_exists($path)) {
            return false;
        }

        $output = file_get_contents($path);
        if ($output) {
            return $output;
        }
        return false;
    }

    private static function getKeysFromTpl($input, $kI = '%%')
    {
        if (empty($input)) {
            return false;
        }
        $pattern = '/'.$kI.'([^'.$kI.'.]*)'.$kI.'/';
        preg_match_all($pattern, $input, $keyArray);
        $keys['full']  = $keyArray[0];
        $keys['clean'] = $keyArray[1];

        return $keys;
    }

    private static function replaceKeysInTpl($input, $array, $kI = '%%')
    {
        $keys    = self::getKeysFromTpl($input);
        $search  = null;
        $replace = null;
        $i       = 0;

        if (!empty($keys)) {
            foreach ($keys['clean'] as $key) {
                $search[$i] = $kI.$key.$kI;
                if (array_key_exists($key, $array)) {
                    $replace[$i] = $array[$key];
                } else {
                    $replace[$i] = null;
                }
                $i++;
            }
        }

        return str_replace($search, $replace, $input);
    }

    public static function genOutput($file, $array = array())
    {
        return self::replaceKeysInTpl(self::getTplFile($file), $array);
    }
}
