<?PHP
namespace StaticFusion;

class Settings
{
    public static function getSettings($full)
    {
        global $appDB;

        if ($full) {
            $sql = "SELECT * FROM `settings`";
        } else {
            $sql = "SELECT `key`, `value` FROM `settings`";
        }

        $result = $appDB->fetchAll($sql);

        $array = null;
        if ($result) {
            foreach ($result as $item) {
                if ($full) {
                    $array[$item['group']][$item['key']] = array(
                        'key'   => $item['key'],
                        'name'  => $item['name'],
                        'desc'  => $item['description'],
                        'value' => $item['value']
                    );
                } else {
                    $array[$item['key']] = $item['value'];
                }
            }
        }

        return $array;
    }
}
