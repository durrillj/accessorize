<?PHP
function errorInfo($num)
{
    // Determine error name and type
    switch ($num) {
        case E_ERROR:
            $name = 'E_ERROR';
            $type = 'error';
            break;
        case E_WARNING:
            $name = 'E_WARNING';
            $type = 'warning';
            break;
        case E_PARSE:
            $name = 'E_PARSE';
            $type = 'error';
            break;
        case E_NOTICE:
            $name = 'E_NOTICE';
            $type = 'info';
            break;
        case E_CORE_ERROR:
            $name = 'E_CORE_ERROR';
            $type = 'error';
            break;
        case E_CORE_WARNING:
            $name = 'E_CORE_WARNING';
            $type = 'warning';
            break;
        case E_COMPILE_ERROR:
            $name = 'E_COMPILE_ERROR';
            $type = 'error';
            break;
        case E_COMPILE_WARNING:
            $name = 'E_COMPILE_WARNING';
            $type = 'warning';
            break;
        case E_USER_ERROR:
            $name = 'E_USER_ERROR';
            $type = 'error';
            break;
        case E_USER_WARNING:
            $name = 'E_USER_WARNING';
            $type = 'warning';
            break;
        case E_USER_NOTICE:
            $name = 'E_USER_NOTICE';
            $type = 'info';
            break;
        case E_STRICT:
            $name = 'E_STRICT';
            $type = 'info';
            break;
        case E_RECOVERABLE_ERROR:
            $name = 'E_RECOVERABLE_ERROR';
            $type = 'error';
            break;
        case E_DEPRECATED:
            $name = 'E_DEPRECATED';
            $type = 'warning';
            break;
        case E_USER_DEPRECATED:
            $name = 'E_USER_DEPRECATED';
            $type = 'warning';
            break;
        case E_ALL:
            $name = 'E_ALL';
            $type = 'error';
            break;
        default:
            $name = 'UNKNOWN ERROR';
            $type = 'error';
            break;
    }

    $array['name'] = $name;
    $array['type'] = $type;

    return $array;
}

function logError($e)
{
    switch ($e['type']) {
        case 'error':
            $logFile = LOG_PATH.'errors.log';
            break;
        case 'warning':
            $logFile = LOG_PATH.'warnings.log';
            break;
        case 'info':
            $logFile = LOG_PATH.'notices.log';
            break;
    }
    $time   = date("Y-m-d h:i:s A");
    $name   = $e['name'];
    $num    = $e['num'];
    $str    = $e['str'];
    $file   = $e['file'];
    $line   = $e['line'];
    $output = '['.$time.'] '.$name.'('.$num.'): '.trim($str).' in '.$file.' on line '.$line."\r\n";

    // Write log entry to the log
    if (file_put_contents($logFile, $output, FILE_APPEND | LOCK_EX) === false) {
        return false;
    }
    return true;
}

function customErrorHandler($num, $str, $file = null, $line = null, $trace = null)
{
    // Make global variables available
    global $errors;

    $eInfo     = errorInfo($num);
    $e['type'] = $eInfo['type'];
    $e['name'] = $eInfo['name'];
    $e['num']  = $num;
    $e['str']  = htmlentities($str);
    $e['file'] = $file;
    $e['line'] = $line;

    $errors[$e['type']][] = $e;

    $log_result = logError($e);

    return 0;
}

function customShutdown()
{
    $errorTemplate = TPL_PATH.'/errors/500.tpl';
    $logFile       = LOG_PATH.'/errors.log';
    $lastError     = error_get_last();
    if ($lastError !== null) {
        $eInfo     = errorInfo($lastError['type']);
        $e['type'] = $eInfo['type'];
        $e['name'] = $eInfo['name'];
        $e['num']  = $lastError['type'];
        $e['str']  = $lastError['message'];
        $e['file'] = $lastError['file'];
        $e['line'] = $lastError['line'];

        logError($e);

        // Display Server Error page
        if (file_exists($errorTemplate)) {
            $html = file_get_contents($errorTemplate);
        } else {
            $html = '<h1>500 SERVER ERROR</h1>';
        }
        print($html);
        http_response_code(500);
        exit;
    }
}
