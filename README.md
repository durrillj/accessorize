accessorize
===========

This is the source code for http://accessorizeeveryone.net

Current Version: 1.0.0
Content type : Dynamic

Updates
===========
v1.0.0: Base site completed


Future
===========

The goal will be to eventually expand the site to allow product orders through a fully
functional shopping cart.  For now the site is for informational purposes.
