<?PHP

// Set Constants
defined('ROOT_PATH')
    or define('ROOT_PATH', realpath(dirname(__FILE__)).'/');
defined('LIB_PATH')
    or define('LIB_PATH', ROOT_PATH.'lib/');
defined('TPL_PATH')
    or define('TPL_PATH', ROOT_PATH.'tpl/');
defined('DB_PATH')
    or define('DB_PATH', ROOT_PATH.'db/');
defined('LOG_PATH')
    or define('LOG_PATH', ROOT_PATH.'logs/');

// Load environment configuration
require_once(ROOT_PATH.'/env.conf.php');

// Setup custom error handler
require_once(LIB_PATH.'errorhandler.php');
set_error_handler('customErrorHandler');
register_shutdown_function('customShutdown');

// Auto load class files
set_include_path(get_include_path().PATH_SEPARATOR.LIB_PATH);
spl_autoload_extensions('.class.php');
spl_autoload_register();
