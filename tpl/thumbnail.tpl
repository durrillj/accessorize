<div class="col-xs-4">
    <a href="" class="thumbnail" data-toggle="modal" data-target="#%%target%%">
        <img src="%%file%%">
    </a>
    <div class="modal fade" id="%%target%%" tabindex="-1" role="dialog" aria-labelledby="%%label%%">
        <div class="lightbox" role="document">
            <div class="img_container">
                <img src="%%file%%">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
        </div>
    </div>
</div>
