
            <div class="container body_container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="body_content">
                            <span>Thank you for taking the time to check out our promotions page.</span>
                        </p>
                        <br>
                        <div class="col-md-8 col-md-offset-2">
                            <a class="coupon" href="/coupon">
                                <h1>GET 15% OFF</h1>
                                <h4>ON YOUR PURCHASE</h4>
                                <span class="coupon-body">Print this coupon or present this page on your phone to the Sales Representative at the point of sale to receive 15% off of your purchase**</span>
                                <span class="coupon-footer">
                                    <span class="coupon-ltext pull-left">
                                        <span>Offer Expires: 01/31/2017</span><br>
                                        <span>** Excludes Otterbox, UAG, and Bluetooth products.</span>
                                    </span>
                                    <span class="coupon-rtext pull-right hidden-xs">COUPON</span>
                                </span>
                            </a>
                            <span class="coupon-instructions">Click the coupon above for a printer friendly version!</span>
                        </div>
                    </div>
                </div>
            </div>
