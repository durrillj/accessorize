<div class="container body_container">
    <div class="row">
        <div class="col-sm-9">
            <div class="products_list">
                <table class="product-table">
                    <tr>
                        <td class="product-table-brand"><strong>Body Glove</strong></td>
                        <td class="product-table-desciption">Snap on Covers with Swivel Clip</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>LifeProof</strong></td>
                        <td class="product-table-desciption">LifeProof Water Proof Case Product Lines</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>MyBat</strong></td>
                        <td class="product-table-desciption">Screen Protectors (Tempered Glass), Faceplates</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>NFL</strong></td>
                        <td class="product-table-desciption">Officially Licensed NFL Phone Cases</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>Naztech</strong></td>
                        <td class="product-table-desciption">Waterproof Cases and Power Solutions</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>NiteIze</strong></td>
                        <td class="product-table-desciption">Connect Series Cases and Pouches</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>Nuglas</strong></td>
                        <td class="product-table-desciption">Screen Protectors (Tempered Glass)</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>OtterBox</strong></td>
                        <td class="product-table-desciption">Symmetry, Defender, Commuter Product Lines</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>Qmadix</strong></td>
                        <td class="product-table-desciption">Premium Car Chargers, Home Chargers, Tempered Glass Screen Protectors</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>Reiko</strong></td>
                        <td class="product-table-desciption">USB Cables and Pouches</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>Trackform</strong></td>
                        <td class="product-table-desciption">Car Mounts</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>UAG - Urban Armor Gear</strong>
                        </td><td class="product-table-desciption">Composite Phone Cases</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>BlueAnt</strong></td>
                        <td class="product-table-desciption">BlueAnt Bluetooth Product Lines</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>LG</strong></td>
                        <td class="product-table-desciption">Assorted Bluetooth Headsets</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>Motorola</strong></td>
                        <td class="product-table-desciption">Motorola Bluetooth Product Lines</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>Plantronics</strong></td>
                        <td class="product-table-desciption">Plantronics Bluetooth Product Lines</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>Samsung Mobility</strong></td>
                        <td class="product-table-desciption">Samsung Bluetooth Product Lines</td>
                    </tr>
                    <tr>
                        <td class="product-table-brand"><strong>Supertooth</strong></td>
                        <td class="product-table-desciption">Supertooth Bluetooth Product Lines</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="products_img_container">
                <div class="products_img">
                    <img src="/assets/img/product_1_otterbox_defender.jpg">
                </div>
                <div class="products_img">
                    <img src="/assets/img/product_2_lifeproof_fre.jpg">
                </div>
                <div class="products_img">
                    <img src="/assets/img/product_3_UAG.jpg">
                </div>
            </div>
        </div>
    </div>
</div>

())(
)(()
