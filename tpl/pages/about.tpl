                <div class="container body_container">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="about_image_container">
                                <img class="about_image" src="/assets/img/about.jpg">
                            </p>
                            <p class="body_content">It’s all about our clients’ experiences here at The Accessory Store. We don’t do junk. Everything in our store is tested for quality and affordably priced to earn your trust. There’s no waiting for shipping, paying for handling, no buyer’s remorse. We simply offer the best accessories you need in the friendly, hands-on, no-pressure environment you’ve been looking for.</p>
                            <p class="body_content">Our deep-rooted experience and entrepreneurial spirit comes from over 18 years in the wireless industry. Our owner, Aaron Wooley, “grew up” in the wireless business; dedicating over 11 years to corporate purchasing and sales. Then he opened up his own store where he could personally interact with his clients and give them the impeccable sales and service that he himself was so passionate about.</p>
                            <p class="body_content">Today, The Accessory Store has over 3300 square feet of product, at some of the best prices in the Sacramento, CA area, including headphones, lanyards, car mounts, batteries, keyboards, carrying bags, pouches, faceplates, and so much more. We provide a personalized touch that can only be found in our store, and not some online checkout. At The Accessory Store, we are your local resource for any and all cellular accents.</p>
                        </div>
                    </div>
                </div>
