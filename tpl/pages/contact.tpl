<div class="container body_container">
    <div class="row">
        <div class="col-md-6">
            <h2 class="sub_header">The Accessory Store</h2>
            <p class="body_content">8460 Elk Grove Blvd Suite 100<br>Elk Grove, Ca 95758</p>
            <p class="body_content"><strong>Phone:</strong> (916) 226-6917</p>
            <p class="body_content"><strong>Email:</strong> <a class="contact_link" href="mailto:contactus@accessorizeeveryone.net">contactus@accessorizeeveryone.net</a></p>
        </div>
        <div class="col-md-6">
            <h2 class="sub_header" style="text-align: center">Hours</h2>
            <p class="body_content" style="text-align: center">Monday-Friday: 11:00 am to 8:00 pm</p>
            <p class="body_content" style="text-align: center">Saturday: 11:00 am to 7:00 pm</p>
            <p class="body_content" style="text-align: center">Sunday: 12:00 noon to 5:00 pm</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3">
            <div class="map_wrapper">
                %%iframe%%
            </div>
        </div>
    </div>
</div>
