<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- =================================================================== -->
        <!-- META                                                                -->
        <!-- =================================================================== -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        %%meta%%
        <!-- =================================================================== -->
        <!-- Title                                                               -->
        <!-- =================================================================== -->
        <title>The Accessory Store - %%title%%</title>

        <!-- =================================================================== -->
        <!-- CSS                                                                 -->
        <!-- =================================================================== -->
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        %%page_css%%
        <!-- =================================================================== -->
        <!-- FONT                                                                -->
        <!-- =================================================================== -->
        <link href="http://fonts.googleapis.com/css?family=Rokkitt:400,700" rel="stylesheet" type="text/css">

        <!-- =================================================================== -->
        <!-- HTML5 Shim and Respond.js IE8 support                               -->
        <!-- of HTML5 elements and media queries                                 -->
        <!-- WARNING: Respond.js doesn\'t work if                                -->
        <!-- you view the page via file://                                       -->
        <!-- =================================================================== -->

        <!--[if lt IE 9]>
            <script src="/assets/js/html5shiv.min.js"></script>
            <script src="/assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- =================================================================== -->
        <!-- Google Analytics                                                    -->
        <!-- =================================================================== -->
        %%analytics%%
    </head>
    <body>
        <div class="content-wrapper">
            <div class="container">
                <div class="col-md-8 col-md-offset-2" style="text-align: right">
                    <a id="print-btn" class="btn btn-xs btn-primary" href="javascript:window.print()">Print</a>
                    <span class="coupon" id="print-this">
                        <h1>GET 15&#37; OFF</h1>
                        <h4>ON YOUR PURCHASE</h4>
                        <span class="coupon-body">Print this coupon or present this page on your phone to the Sales Representative at the point of sale to receive 15&#37; off of your purchase**</span>
                        <span class="coupon-footer">
                            <span class="coupon-ltext pull-left">
                                <span>Offer Expires: 01/31/2017</span><br>
                                <span>** Excludes Otterbox, UAG, and Bluetooth products.</span>
                            </span>
                            <span class="coupon-rtext pull-right hidden-xs">COUPON</span>
                        </span>
                    </span>
                    <a id="back-btn" class="btn btn-xs btn-success" href="/promotions">Back</a>
                </div>
                <div class="content-push"></div>
            </div>
        </div>
        <!-- =================================================================== -->
        <!-- JavaScript                                                          -->
        <!-- =================================================================== -->
        <script src="/assets/js/jquery-3.1.1.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        %%page_js%%
    </body>
</html>
