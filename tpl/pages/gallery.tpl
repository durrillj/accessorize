<div class="container body_container">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <h2 class="sub_header" style="text-align: center;">
                <span>Store</span>
            </h2>
            %%store_thumbs%%
        </div>
        <div class="col-xs-12 col-md-6">
            <h2 class="sub_header" style="text-align: center;">
                <span>Products</span>
            </h2>
            %%product_thumbs%%
        </div>
    </div>
</div>
