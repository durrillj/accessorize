
                <div class="container body_container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="home_image_container">
                                <img class="home_image" src="/assets/img/home.jpg">
                            </p>
                            <h2 class="sub_header">
                                <span>Welcome to Sacramento's largest cellphone accessory emporium!</span>
                            </h2>
                            <p class="body_content">
                                <span>We sell a wide variety of the latest cellphone accessories and gear from top names like Otter Box, LifeProof, UAG - Urban Armor Gear, HyperGear, Samsung, Motorola, LG, and more. We have an almost unlimited inventory to choose from, such as domestic, rare, hard-to-find, discontinued items, and international imports. All of our products are made to fit the broad spectrum of cell phones, iPhones, Androids, Galaxies, Blackberries, and other devices.</span>
                            </p>
                            <p class="body_content">
                                <span>From start to finish, the dedicated sales and service staff at The Accessory Store strives to live up to modern expectations and help you make the right choices to help you celebrate your unique tastes and personality. We’ll even let you choose and model the different faceplates to get the right look and feel. At our store, accessorizing feels like choosing the right outfit for your phone!</span>
                            </p>
                            <p class="body_content">
                                <span>If you have a new or existing device, working with our service experts can safeguard your phone. We install premium screen protectors without annoying air bubbles or imperfections. This protection will keep your glass from shattering, prevent replacing your phone with every accident, and save you money. By combining our comprehensive inventory and impeccable service standards, we can provide you with the best accessory experience in Northern California. </span>
                            </p>
                        </div>
                    </div>
                </div>
