
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="main-navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="%%home%% nav_item"><a href="/">Home</a></li>
                            <li class="%%about_us%% nav_item"><a href="/about_us">About Us</a></li>
                            <li class="%%products%% nav_item"><a href="/products">Products</a></li>
                            <li class="%%gallery%% nav_item"><a href="/gallery">Gallery</a></li>
                            <li class="%%promotions%% nav_item"><a href="/promotions">Promotions</a></li>
                            <li class="%%contact_us%% nav_item"><a href="/contact_us">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
