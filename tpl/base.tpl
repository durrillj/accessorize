<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- =================================================================== -->
        <!-- META                                                                -->
        <!-- =================================================================== -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        %%meta%%
        <!-- =================================================================== -->
        <!-- Title                                                               -->
        <!-- =================================================================== -->
        <title>The Accessory Store - %%title%%</title>

        <!-- =================================================================== -->
        <!-- CSS                                                                 -->
        <!-- =================================================================== -->
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        <link href="/assets/css/main.css?version=1.0.0" rel="stylesheet">
        %%page_css%%
        <!-- =================================================================== -->
        <!-- FONT                                                                -->
        <!-- =================================================================== -->
        <link href="http://fonts.googleapis.com/css?family=Rokkitt:400,700" rel="stylesheet" type="text/css">

        <!-- =================================================================== -->
        <!-- HTML5 Shim and Respond.js IE8 support                               -->
        <!-- of HTML5 elements and media queries                                 -->
        <!-- WARNING: Respond.js doesn\'t work if                                -->
        <!-- you view the page via file://                                       -->
        <!-- =================================================================== -->

        <!--[if lt IE 9]>
            <script src="/assets/js/html5shiv.min.js"></script>
            <script src="/assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- =================================================================== -->
        <!-- Google Analytics                                                    -->
        <!-- =================================================================== -->
        %%analytics%%
    </head>
    <body>
        <div class="content-wrapper">
            <!-- ================================================================= -->
            <!-- Header Banner                                                     -->
            <!-- ================================================================= -->
            <header>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="logo pull-left">
                                <img class="logo" src="/assets/img/accessorize_logo.gif">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="social_icons pull-right">
                                        <ul>
                                            <li><a href="https://www.facebook.com/pages/The-Accessory-Store-Elkgrove/221508694533582" target="_blank"><img src="/assets/img/facebook_icon.png"></a></li>
                                            <li><a href="https://twitter.com/AccessorizEvry2" target="_blank"><img src="/assets/img/twitter_icon.png"></a></li>
                                            <li><a href="https://plus.google.com/u/1/113249016282778331557/about?gl=us&amp;hl=en" target="_blank"><img src="/assets/img/google_plus_icon.png"></a></li>
                                            <li><a href="https://foursquare.com/v/the-accessory-store/4fd26b44e4b013c5801cb01a" target="_blank"><img src="/assets/img/foursquare_icon.png"></a></li>
                                            <li><a href="http://www.yellowpages.com/elk-grove-ca/mip/accessory-store-464689321" target="_blank"><img src="/assets/img/yp_icon.png"></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="cta_box">
                                        <p>Call or stop by today! <strong>(916) 226-6917</strong><headeraddress>8460 Elk Grove Blvd Suite 100<br> Elk Grove, CA 95758</headeraddress></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- ================================================================= -->
            <!-- Nav Bar                                                           -->
            <!-- ================================================================= -->
            %%nav%%
            <!-- ================================================================= -->
            <!-- Page Content                                                      -->
            <!-- ================================================================= -->
            %%body%%
            <div class="content-push"></div>
        </div>
        <!-- =================================================================== -->
        <!-- Footer                                                              -->
        <!-- =================================================================== -->
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">
                            <p>&copy; 2015 The Accessory Store</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- =================================================================== -->
        <!-- JavaScript                                                          -->
        <!-- =================================================================== -->
        <script src="/assets/js/jquery-3.1.1.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        %%page_js%%
    </body>
</html>
